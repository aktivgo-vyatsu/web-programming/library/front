const API_LIBRARY_ADDRESS = 'http://api.library.loc'

Vue.component('modal-sign-in', {
    data() {
        return {
            params: {
                login: '',
                password: '',
            }
        }
    },
    template: `
        <transition name="modal">
            <div class="modal-mask">
              <div class="modal-wrapper">
                <div class="modal-container">

                  <div class="modal-header">
                    <h1>Вход</h1>
                  </div>

                  <div class="modal-body">
                    <label class="m-2 w-auto">Логин<input type="text" v-model="params.login" class="m-2 w-auto"></label>
                    <label class="m-2 w-auto">Пароль<input type="password" v-model="params.password" class="m-2 w-auto"></label>
                  </div>

                  <div class="modal-footer">
                    <button class="modal-default-button" @click="$emit('close')">
                      Отмена
                    </button>
                    <button class="modal-default-button" @click="signIn">
                      Войти
                    </button>
                  </div>
                </div>
              </div>
            </div>
        </transition>
    `,
    methods: {
        signIn() {
            if (this.params.login === '') {
                alert('Проверьте корректность поля Логин')
                return
            }
            if (this.params.password === '') {
                alert('Проверьте корректность поля Пароль')
                return
            }
            this.$emit('signIn', this.params)
        }
    }
})

Vue.component('modal-sign-up', {
    data() {
        return {
            params: {
                login: '',
                password: '',
                role: 'consumer',
            }
        }
    },
    template: `
        <transition name="modal">
            <div class="modal-mask">
              <div class="modal-wrapper">
                <div class="modal-container">

                  <div class="modal-header">
                    <h1>Регистрация</h1>
                  </div>

                  <div class="modal-body">
                    <label class="m-2 w-auto">Логин<input type="text" v-model="params.login" class="m-2 w-auto"></label>
                    <label class="m-2 w-auto">Пароль<input type="password" v-model="params.password" class="m-2 w-auto"></label>
                    <label class="m-2 w-auto">Роль<select v-model="params.role" class="m-2 w-auto">
                      <option>administrator</option>
                      <option selected>consumer</option>
                    </select></label>
                  </div>

                  <div class="modal-footer">
                    <button class="modal-default-button" @click="$emit('close')">
                      Отмена
                    </button>
                    <button class="modal-default-button" @click="singUp">
                      Зарегистрироваться
                    </button>
                  </div>
                </div>
              </div>
            </div>
        </transition>
    `,
    methods: {
        singUp() {
            if (this.params.login === '') {
                alert('Проверьте корректность поля Логин')
                return
            }
            if (this.params.password === '') {
                alert('Проверьте корректность поля Пароль')
                return
            }
            if (this.params.role === '') {
                alert('Проверьте корректность поля Роль')
                return
            }
            this.$emit('signUp', this.params)
        }
    }
})

Vue.component('modal-add-book', {
    data() {
        return {
            book: {
                title: '',
                author: '',
                genre: '',
                price: '',
                description: '',
                image: '',
            },
        }
    },
    template: `
        <transition name="modal">
        <div class="modal-mask">
          <div class="modal-wrapper">
            <div class="modal-container">

              <div class="modal-header">
                <h1>Добавление книги</h1>
              </div>

              <div class="modal-body">
                <label class="m-2 w-auto">Название<input type="text" v-model="book.title" class="m-2 w-auto"></label>
                <label class="m-2 w-auto">Автор<input type="text" v-model="book.author" class="m-2 w-auto"></label>
                <label class="m-2 w-auto">Жанр<input type="text" v-model="book.genre" class="m-2 w-auto"></label>
                <label class="m-2 w-auto">Цена<input type="text" v-model="book.price" class="m-2 w-auto"></label>
                <label class="m-2 w-auto">Описание<input type="text" v-model="book.description" class="m-2 w-auto"></label>
                <label class="m-2 w-auto">Изображение<input type="text" v-model="book.image" class="m-2 w-auto"></label>
              </div>

              <div class="modal-footer">
                  <button class="modal-default-button" @click="$emit('close')">
                    Отмена
                  </button>
                  <button class="modal-default-button" @click="save">
                    Сохранить
                  </button>
              </div>
            </div>
          </div>
        </div>
      </transition>
    `,
    methods: {
        save() {
            if (this.book.title === '') {
                alert('Проверьте корректность поля Название')
                return
            }
            if (this.book.author === '') {
                alert('Проверьте корректность поля Автор')
                return
            }
            if (this.book.genre === '') {
                alert('Проверьте корректность поля Жанр')
                return
            }
            this.book.price = parseFloat(this.book.price)
            if (this.book.price === 0) {
                this.book.price = ''
                alert('Проверьте корректность поля Цена')
                return
            }
            if (this.book.description === '') {
                alert('Проверьте корректность поля Описание')
                return
            }
            if (this.book.image === '') {
                alert('Проверьте корректность поля Изображение')
                return
            }
            this.$emit('createBook', this.book)
        }
    }
})

Vue.component('modal-edit-book', {
    data() {
        return {
            book: {
                id: '',
                title: '',
                author: '',
                genre: '',
                price: '',
                description: '',
                image: '',
            }
        }
    },
    template: `
      <transition name="modal">
      <div class="modal-mask">
        <div class="modal-wrapper">
          <div class="modal-container">

            <div class="modal-header">
              <h1>Редактирование книги</h1>
            </div>

            <div class="modal-body">
              <label class="m-2 w-auto">Название<input type="text" v-model="book.title" class="m-2 w-auto"></label>
              <label class="m-2 w-auto">Автор<input type="text" v-model="book.author" class="m-2 w-auto"></label>
              <label class="m-2 w-auto">Жанр<input type="text" v-model="book.genre" class="m-2 w-auto"></label>
              <label class="m-2 w-auto">Цена<input type="text" v-model="book.price" class="m-2 w-auto"></label>
              <label class="m-2 w-auto">Описание<input type="text" v-model="book.description" class="m-2 w-auto"></label>
              <label class="m-2 w-auto">Изображение<input type="text" v-model="book.image" class="m-2 w-auto"></label>
            </div>

            <div class="modal-footer">
              <button class="modal-default-button" @click="$emit('close')">
                Отмена
              </button>
              <button class="modal-default-button" @click="save">
                Сохранить
              </button>
            </div>
          </div>
        </div>
      </div>
      </transition>
    `,
    methods: {
        init(book) {
            this.book = book
        },
        save() {
            this.book.price = parseFloat(this.book.price)
            this.$emit('updateBook', this.book)
        }
    }
})

Vue.component('book-card', {
    props: {
        book: {
            type: Object,
            required: true
        },
        is_admin: {
            type: Boolean,
            required: true
        }
    },
    template: `
        <div class="card shadow-sm border-dark h-100" style="padding: 12px">
            <div v-show="is_admin" class="card-header mb-2">
              <div class="btn-group w-100">
                <button @click="$emit('edit-book')" ref="modal-edit-book" id="btn-edit" type="button" class="btn btn-sm btn-outline-warning">Редактировать</button>
                <button @click="$emit('delete-book')" id="btn-delete" type="button" class="btn btn-sm btn-outline-danger">Удалить</button>
              </div>
            </div>
            <img :src="book.image" class="bd-placeholder-img card-img-top" alt="">
            <div class="card-body">
              <h3 class="card-title">{{book.title}}</h3>
              <h5 class="card-title">{{book.author}}</h5>
              <h3 class="card-title">{{book.price}} &#8381;</h3>
              <p class="card-text">{{book.description}}</p>
            </div>
            <div class="d-flex justify-content-center">
              <div class="btn-group w-100">
                <button type="button" class="btn btn-sm btn-outline-primary">Купить</button>
              </div>
            </div>
        </div>
    `
})

Vue.component('book-catalog', {
    data() {
        return {
            login: '',
            role: '',
            offset: 0,
            limit: 11,
            books: [],
            genres: [
                {
                    'ru_name': 'Все',
                    'en_name': 'all',
                },
                {
                    'ru_name': 'Приключения',
                    'en_name': 'adventures',
                },
                {
                    'ru_name': 'Драма',
                    'en_name': 'drama',
                },
                {
                    'ru_name': 'Фэнтези',
                    'en_name': 'fantasy',
                },
                {
                    'ru_name': 'Детектив',
                    'en_name': 'detective',
                },
                {
                    'ru_name': 'Триллер',
                    'en_name': 'thriller',
                },
                {
                    'ru_name': 'Фантастика',
                    'en_name': 'fantastic',
                },
            ],
            currentGenre: 'all',
            search: '',
            showSigninModal: false,
            showSignupModal: false,
            showAddModal: false,
            showEditModal: false,
            editedBookIndex: 0
        }
    },
    template: `
      <div>
      <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container">
          <a href="#" class="navbar-brand d-flex align-items-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-book"
                 viewBox="0 0 16 16">
              <path
                  d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
            </svg>
            <strong style="margin-left: 20px">Уютная библиотека</strong>
          </a>
          <form class="d-flex">
            <input v-model="search" class="form-control mr-2" type="search" placeholder="Поиск" aria-label="Search">
          </form>
          <div class="d-flex justify-content-center">
            <div class="btn-group w-100">
              <button v-show="!logged" @click="showSigninModal = true" id="btn-sign-in" type="button" class="btn btn-sm btn-outline-primary">
                Войти
              </button>
              <button v-show="!logged" @click="showSignupModal = true" id="btn-sign-up" type="button" class="btn btn-sm btn-outline-primary">
                Зарегистрироваться
              </button>
              <button v-show="logged" @click="logOut" id="btn-sign-up" type="button" class="btn btn-sm btn-outline-primary">
                Выйти
              </button>
            </div>
          </div>
        </div>
      </div>

      <modal-sign-in
          ref="modal-sign-in"
          v-show="showSigninModal"
          @signIn="signIn"
          @close="showSigninModal=false"
      ></modal-sign-in>
      <modal-sign-up
          ref="modal-sign-up"
          v-show="showSignupModal"
          @signUp="signUp"
          @close="showSignupModal=false"
      ></modal-sign-up>
      <modal-add-book
          ref="modal-add-book"
          v-show="isAdmin && showAddModal"
          @createBook="createBook"
          @close="showAddModal=false"
      ></modal-add-book>
      <modal-edit-book
          ref="modal-edit-book"
          v-show="isAdmin && showEditModal"
          @updateBook="updateBook"
          @close="showEditModal=false"
      ></modal-edit-book>

      <main>
        <div class="album py-5 bg-light">
          <div class="container">
            <b>{{ login }}</b>
            <b>{{ role }}</b>
            <ol class="breadcrumb">
              <li v-for="genre in genres" @click="selectGenre(genre)" class="breadcrumb-item active"
                  aria-current="page">
                <a href="#" v-if="genre.en_name !== currentGenre">{{ genre.ru_name }}</a>
                <b v-else>{{ genre.ru_name }}</b>
              </li>
            </ol>
            <button v-show="isAdmin" @click="showAddModal = true" id="btn-add-book" type="button" class="btn btn-outline-primary mb-3">
              Добавить книгу
            </button>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              <div class="col" v-for="(book, index) in filteredBooks">
                <book-card
                    @edit-book="editBook(index)"
                    @delete-book="deleteBook(index)"
                    :book="book"
                    :is_admin="isAdmin"
                ></book-card>
              </div>
              <button @click="getBooks()" id="btn-get-more" type="button" class="btn btn-outline-primary mb-3">
                Загрузить ещё
              </button>
            </div>
          </div>
        </div>
      </main>
      </div>
    `,
    methods: {
        getBooks(offset = this.offset, limit = this.limit) {
            fetch(API_LIBRARY_ADDRESS + `/books?offset=${offset}&limit=${limit}`)
                .then(resp => {
                    if (resp.status !== 200) return
                    resp.json().then(data => {
                        if (data.length === 0 || data.length < limit) {
                            document.getElementById('btn-get-more').setAttribute('hidden', 'true')
                        }
                        this.books.push(...data)
                        this.offset += limit
                    })
                })
                .catch(err => console.log(err))
        },
        initAuth() {
            fetch(API_LIBRARY_ADDRESS + '/auth/me', {
                method: 'GET',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
            }).then(resp => {
                if (resp.status !== 200) return
                resp.json().then(data => {
                    this.login = data.login
                    this.role = data.role
                })
            }).catch(err => console.log(err))
        },
        signIn(params) {
            console.log(params)
            fetch(API_LIBRARY_ADDRESS + '/auth/signin', {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(params)
            }).then(resp => {
                if (resp.status !== 200) {
                    resp.json().then(data => {
                        alert(JSON.stringify(data))
                    })
                    return
                }
                resp.json().then(data => {
                    this.login = data.login
                    this.role = data.role
                })
                this.showSigninModal = false
            }).catch(err => console.log(err))
        },
        signUp(params) {
            fetch(API_LIBRARY_ADDRESS + '/auth/signup', {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(params)
            }).then(resp => {
                if (resp.status !== 201) {
                    resp.json().then(data => {
                        alert(JSON.stringify(data))
                    })
                    return
                }
                resp.json().then(data => {
                    this.login = data.login
                    this.role = data.role
                })
                this.showSignupModal = false
            }).catch(err => console.log(err))
        },
        logOut() {
            fetch(API_LIBRARY_ADDRESS + '/auth/signout', {
                credentials: 'include',
            }).then(resp => {
                if (resp.status !== 204) {
                    resp.json().then(data => {
                        alert(JSON.stringify(data))
                    })
                    return
                }
                this.login = ''
                this.role = ''
            }).catch(err => console.log(err))
        },
        selectGenre(genre) {
            this.currentGenre = genre.en_name
            let getMoreBtn = document.getElementById('btn-get-more')
            if (this.currentGenre === 'all') getMoreBtn.removeAttribute('hidden')
            else getMoreBtn.setAttribute('hidden', 'true')
        },
        createBook(book) {
            console.log(book)
            fetch(API_LIBRARY_ADDRESS + '/books', {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(book)
            }).then(resp => {
                if (resp.status !== 201) {
                    resp.json().then(data => {
                        alert(JSON.stringify(data))
                    })
                    return
                }
                resp.json().then(data => {
                    book.id = data.id
                    this.books.push(book)
                })
                this.showAddModal = false
            }).catch(err => console.log(err))
        },
        editBook(bookIndex) {
            this.editedBookIndex = bookIndex
            this.$refs['modal-edit-book'].init(this.books[bookIndex])
            this.showEditModal = true
        },
        updateBook(book) {
            let data = {
                title: book.title,
                author: book.author,
                genre: book.genre,
                price: parseFloat(book.price),
                description: book.description,
                image: book.image,
            }
            fetch(API_LIBRARY_ADDRESS + '/books/' +  this.books[this.editedBookIndex].id, {
                method: 'PATCH',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            }).then(resp => {
                if (resp.status !== 200) {
                    resp.json().then(data => {
                        alert(JSON.stringify(data))
                    })
                    return
                }
                this.books[this.editedBookIndex].title = book.title
                this.books[this.editedBookIndex].author = book.author
                this.books[this.editedBookIndex].genre = book.genre
                this.books[this.editedBookIndex].price = book.price
                this.books[this.editedBookIndex].description = book.description
                this.books[this.editedBookIndex].image = book.image
                this.showEditModal = false
            }).catch(err => console.log(err))
        },
        deleteBook(bookIndex) {
            if (!confirm('Вы уверены?')) return
            fetch(API_LIBRARY_ADDRESS + '/books/' +  this.books[bookIndex].id, {
                method: 'DELETE',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
            }).then(resp => {
                if (resp.status !== 204) {
                    resp.json().then(data => {
                        alert(JSON.stringify(data))
                    })
                    return
                }
                this.books.splice(bookIndex, 1)
            }).catch(err => console.log(err))
        },
    },
    computed: {
        filteredBooks() {
          return this.books.filter(book => ((book.title.indexOf(this.search) !== -1) && (book.genre === this.currentGenre || this.currentGenre === 'all')))
        },
        isAdmin() {
            return this.role === 'administrator'
        },
        logged() {
            return this.login !== ''
        }
    },
    created:  function() {
        this.getBooks()
        this.initAuth()
    },
})

let app = new Vue({
    el: '#app',
})